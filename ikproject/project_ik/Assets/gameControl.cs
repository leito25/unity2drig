﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameControl : MonoBehaviour
{
    public Animator my_animator;
    public GameObject my_gameCharacter;

    bool idle = true;
    bool run = false;
    bool attack = false;

    // Start is called before the first frame update
    void Start()
    {
        
        

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            run = !run;
            my_animator.SetBool("Run", run);

        }else if (Input.GetMouseButtonDown(1))
        {
            attack = !attack;
            my_animator.SetBool("Attack", attack);
        }

    }
}
